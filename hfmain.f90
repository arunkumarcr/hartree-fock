program hfmain
use general_data
use basis_data
! This program solves Hartree Fock equations for two center two electron systems
! It uses only STO-3G minimal basis
! So far this is not generalized for bigger systesm.

!integer 						:: Za, Zb, num_atoms, num_prim_gauss
!	double precision, allocatable  	:: coeff(:,:), expon(:,:), phi_1s(:)
!double precision, allocatable	:: nuc_coord(:)
implicit double precision (a-h,o-z)
integer :: i, j , p , q
double precision, allocatable :: S(:,:), K(:,:)


!interface
!    function overlap(exponent1, exponent2)
!        double precision:: exponent1(3,2), exponent2(3,2)
!    end function
!end interface

! interface
!     subroutine printing(message, matrix)
!         character(len=7) :: message
!         double precision, allocatable:: matrix(:,:)
!     end subroutine
! end interface



allocate(S(num_atoms,num_atoms), K(num_atoms,num_atoms))

S = 0.d0 
K = 0.d0
!scalling of exponents and coefficients
do i = 1,3
  expon(i,1) = expon(i,1)*zeta_H**2
  expon(i,2) = expon(i,2)*zeta_He**2
  coeff(i,1) = coeff(i,1)*(2.0*expon(i,1)/pi)**0.75d0
  coeff(i,2) = coeff(i,2)*(2.0*expon(i,2)/pi)**0.75d0
enddo

!print*, "Main first set exponents", expon(:,1)
!print*, "Main secon set exponents", expon(:,2)
!print*, "Main first set coefficients", coeff(:,1)
!print*, "Main second set coefficients", coeff(:,2)


!do i = 1, 2
!	do j =1,2
		do p = 1, 3
		    do q = 1, 3
                alpha = expon(p,1); beta = expon(q,2)
                S(1,1) = S(1,1) + coeff(p,1)*coeff(q,2)*(pi/(coeff(p,1)+coeff(q,2)))**1.5*dexp(-(coeff(p,1)*coeff(q,2))*(rAB2)/(coeff(p,1)+coeff(q,2)))! overlap(expon(p,1),expon(q,1))
!                S(2,2) = S(2,2) + coeff(p,1)*coeff(q,2)*overlap(alpha,beta)
                print*, "full expression", (pi/(coeff(p,1)+coeff(q,2)))**1.5*dexp(-(coeff(p,1)*coeff(q,2))*(rAB2)/(coeff(p,1)+coeff(q,2)))! overlap(expon(p,1),expon(q,1))
                print*, "from function", overlap(alpha,beta)
!print*, "from main" , S(1,1), S(2,2)
		    enddo
		enddo
!    enddo
!enddo
print*, "S11", S(1,1)

!call printing ("overlap", S)
! Evaluate overlap matrix here
!call overlap(S)
!call kinetic(K)


!Evaluate kinetic energy


!Evaluate potential energy


!Evaluate nucleus-nucleus interaction

!Evaluate one-electron integral


!Evaluate two-electron integral



end program
