
! this is two electron integral part
double precision function two_electron_integral(alpha, beta, gamma1, delta, squared_rab, squared_rcd, squared_rpq)
use general_data, only : pi
double precision :: alpha, beta, gamma1, delta, F0
double precision, intent(in) :: squared_rab, squared_rcd, squared_rpq
two_electron_integral = (2.d0*pi**2.5d0)/((alpha+beta)*(gamma1+delta)*dsqrt(alpha+beta+gamma1+delta))*F0((alpha+beta)*(gamma1+delta)*squared_rpq/(alpha+beta+gamma1+delta))*dexp(-(alpha*beta)*(squared_rab)/(alpha+beta)-(gamma1*delta)*(squared_rcd)/(gamma1+delta))
end function
