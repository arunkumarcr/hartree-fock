
double precision function F0(argument)
! This function calculates F0 for (S-type of orbitals)
use general_data, only : pi
double precision :: argument

if (argument < 1.0d-6) then
    F0 = 1.0d0 - argument/3.d0   ! asymptotic value
else
    F0 = 0.5d0*dsqrt(pi/argument)*derf(dsqrt(argument))
endif
end function
