FC = ifort
FFLAGS = -g -O0 



LIBS = -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -liomp5 -lpthread

objects = general_data.o basis_data.o overlap_integral.o kinetic_integral.o nuclear_attraction_integral.o F0.o two_electron_integral.o printing.o diagonalize.o hartree-fock-single.o

hf: $(objects)
	@echo "Making the final executable ball 'hf'"
	$(FC) $(FFLAGS)  -o hf $(objects) $(LIBS) 

%.o: %.f90
	$(FC) $(FFLAGS)  -c $< $(LIBS) 



.PHONY: clean

# Cleaning everything
clean:
	rm $(objects) hf

gzip:
	tar czvf hartree_fock.tar.gz *.f90 README LICENSE Makefile

# End of the makefile