!Program 						
program new						
	implicit none					
	integer						::	true,done, nexponent
	integer						::	gaussian_counter, gaussian_counter_max

	character*200					::	str, ielement_name, basis, folder, str2, str4, trial
	character*200					::	path, coordinate_system, iorbital_name
	character*5					::	ext  
	character*10,allocatable,dimension(:,:)		::	atomic_orbital_name
	character*5					::	element_name(2)

	real(kind(1d0))					::	xcrd(2), ycrd(2), zcrd(2)
	integer						::	natm, iatm
	real(kind(1d0))					::	factor, bondlength
	integer						::	value1, stat
	integer						::	natomic_orbital_max, iexponent, nelement, element_counter
	real(kind(1d0)),allocatable,dimension(:,:)	::	expon, coeff, coeff2
	logical						::	read_basis, file_exists, atomic_gaussians, initial,terminal
	logical						::	flag, element, divider, element_found

	nelement		=	1
	natomic_orbital_max	=	800
	gaussian_counter_max    =       100
	file_exists		=	.false.

	natm		=	2
        element_name(1)	= "H"	
        element_name(2)	= "Li"	

	xcrd(1)	= 0.d0;	ycrd(1)	=0.d0;	zcrd(1)	=0.d0
	xcrd(2)	= 0.d0;	ycrd(2)	=0.d0;	zcrd(1)	=1.4632d0

	bondlength = dsqrt((xcrd(2) - xcrd(1))**2 + (ycrd(2) - ycrd(1))**2 + (zcrd(2) - zcrd(1))**2)

	allocate(atomic_orbital_name(nelement, gaussian_counter_max), stat=done)
	allocate(expon(nelement, natomic_orbital_max), coeff(nelement, natomic_orbital_max), coeff2(nelement, natomic_orbital_max), stat=true)
	if((true .ne. 0) .or. (done .ne. 0)) then
		print*, "mem alloc err!"
		stop
	end if	!true

	print*, "Enter basis set name:"
	read*, basis
	
	folder="/home/thsim1/psi4conda/share/psi4/basis/"
	ext=".gbs"

	path= trim(folder) // trim(basis) // trim(ext)

	inquire(file=trim(path), exist=file_exists)
	if(.not. file_exists) then
		print*, trim(basis),": basis set not found!"
		stop
	end if
	open(1, file=trim(path),  action="read")
	open(2, file="basis.out", action="write")

	element_counter		=	0
	read_basis		=	.true.
	flag			=	.false.
	divider			=	.false.
	initial			= 	.true.
	terminal 		= 	.false. 
	do while (initial)       
		gaussian_counter        =       0
			divider = .false.
		read(1, *) str	
		if((trim(str) .eq. "spherical")	.or. (trim(str) .eq. "cartesian")) then
			coordinate_system = trim(str)     
			write(2, *) trim(basis)," basis with ", trim(coordinate_system)," coordinate system found. . !"
			write(2, *) "Path: ", trim(path)                
		end if
                     
		if(trim(str) .eq. "****") then
			initial = .false.
			exit
		endif
	enddo
	write(2, *) ""
	write(2, *) ""
	write(2, *) "BEGIN reading ", trim(basis), " basis!" 
	flag = .true.

	element_counter	 = element_counter + 1
	nexponent	 = -1
	gaussian_counter = 0

	element = .true.
!	do iatm=1, natm
!		element_found	=	.false.
	do while (.not.terminal)
	        do while(element) 		
				read(1, *) str2, value1
				write(2, *) "=================================================="
				ielement_name = trim(str2)
				write(2, *) "Element: ", trim(ielement_name)
		        	element 	 = .true.
				atomic_gaussians = .true.
				
				do iatm=1, natm
					if(trim(element_name(iatm)) .eq. trim(ielement_name)) element_found = .true.
					if(element_found)	print*, "element found..!", trim(element_name(iatm))
				end do

				do while(atomic_gaussians)
					read(1, *)  iorbital_name, nexponent, factor
!				  	write(2, *), trim(iorbital_name), nexponent, factor
					if(element_found) then
						if(trim(iorbital_name) .ne. "SP") then
							print*, trim(iorbital_name), nexponent, factor
						end if
					end if

					gaussian_counter = gaussian_counter + 1
                		        do iexponent=1, nexponent                             
						if(trim(iorbital_name) .eq. "SP") then
	                	        	    	read(1, *) expon(element_counter, gaussian_counter), coeff(element_counter, gaussian_counter), coeff2(element_counter,gaussian_counter)
        	        	        	    	write(2, *) "		expon= ", expon(element_counter, gaussian_counter), " coeff= ", coeff(element_counter, gaussian_counter), " coeff2= ", coeff2(element_counter,gaussian_counter)
						else if( (trim(iorbital_name) .eq. "S") .or. (trim(iorbital_name) .eq. "P") .or. (trim(iorbital_name) .eq. "D") .or. (trim(iorbital_name) .eq. "F") .or. (trim(iorbital_name) .eq. "G") .or. (trim(iorbital_name) .eq. "H") ) then
							read(1, *) expon(element_counter, gaussian_counter), coeff(element_counter, gaussian_counter)
							write(2, *) "		expon= ",expon(element_counter, gaussian_counter), "coeff= ", coeff(element_counter, gaussian_counter)
							if(element_found) then
								print*, "expon= ", expon(element_counter, gaussian_counter), " coeff= ", coeff(element_counter, gaussian_counter)
							end if
						else
							write(2, *) "Unknown orbital found!"
							atomic_gaussians	=	.false.
							element			=	.false.
							stop 
						end if
	                	        end do	!iexponent

					if((element_found .eq. .true.) .and. (trim(iorbital_name) .eq. "SP")) then
						print*, "S", nexponent, factor
						do iexponent=1, nexponent
							print*, "expon= ", expon(element_counter, gaussian_counter), " coeff = ", coeff(element_counter, gaussian_counter)	
						end do
						print*, "P", nexponent, factor
						do iexponent=1, nexponent
							print*, "expon= ", expon(element_counter, gaussian_counter), " coeff2= ", coeff2(element_counter, gaussian_counter)	
						end do
					end if

					write(2, *) trim(ielement_name), ": orbital= ", trim(iorbital_name)," reading expon/coeff completed."
					write(2, *)
				
					read(1, *) str4
					if(trim(str4) .ne. "****")  then
						backspace(unit=1)
						cycle
					else
	                                         if(trim(str4) .eq. "EOF")  terminal = .true.
						write(2,*) " Completed reading element ", trim(ielement_name)
						exit
!
					end if	
				end do	!atomic_gaussians
					
				read(1,*,iostat=stat) trial
				backspace 1
				!IF(IS_IOSTAT_END(stat)) STOP 'END OF FILE'
				if(is_iostat_end(stat)) then
					write(2, *) "End of File"
					stop "End of file"
				end if
                end do	!element
	enddo		!terminal	
!			if(.not. element_found) print*, trim(element_name(iatm)), " :Element not found!"
!			end do			!iatm

end program new
