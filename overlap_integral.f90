double precision function overlap_integral(alpha,beta,squared_rab)
use general_data, only : pi
double precision :: alpha, beta, squared_rab
overlap_integral = (pi/(alpha+beta))**1.5*dexp(-(alpha*beta)*(squared_rab)/(alpha+beta))
end function
