double precision function kinetic_integral(alpha,beta,squared_rab)
use general_data, only : pi
double precision :: alpha, beta, squared_rab
kinetic_integral = ((alpha*beta)/(alpha+beta))*(3.d0 - 2.d0*alpha*beta*squared_rab/(alpha+beta))*(pi/(alpha+beta))**1.5*dexp(-(alpha*beta)*(squared_rab)/(alpha+beta))
end function