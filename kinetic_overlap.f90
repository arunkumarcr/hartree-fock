subroutine kinetic(k)
! this subroutine calculates the overlap matrix for two center systems
use general_data
use basis_data
implicit double precision (a-h, o-z)
real(kind=DP), intent(out) :: k(2,2)
character(len=60) :: routine
integer:: p,q,i

routine = "Kinetic"
call cpu_time(start)


 = 0.d0


!       For H-H

do p = 1, 3
	do q = 1, 3
		do i = 1, 2
			do j = 1, 2
                overlap(i,j) = overlap(i,j) + scaled_coeff(p,i)*scaled_coeff(q,j)*overlap_integral(scaled_expon(p,i), scaled_expon(q,j),abs(real(i-j))*squared_rab)
                kinetic(i,j) = kinetic(i,j) + scaled_coeff(p,i)*scaled_coeff(q,j)*kinetic_integral(scaled_expon(p,i), scaled_expon(q,j),abs(real(i-j))*squared_rab)
			enddo
		enddo
	enddo
enddo


end subroutine
